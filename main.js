function createNewUser() {
    let user = {
        first_name : prompt("Enter your name"),
        last_name : prompt("Enter your surname"),
        getLogin: function() {
            return (this.first_name[0] + this.last_name).toLocaleLowerCase();
        }
    }

    return user;
}

let userok = createNewUser();

console.log(userok.getLogin());